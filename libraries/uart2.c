#include "uart2.h"
#include "shell.h"

uint8_t uart_rec_buff[100] = {0};
uint8_t uart_rec_buff_cnt = 0;

#if(UART2_INT_EN)

//串口2中断服务程序
//发送数据时,必须关闭中断,这里只负责处理接受中断
void uart2_isr()	interrupt 4
{
	if(RI0)//是串口接受中断
	{
		RI0 = 0;//清除接受中断标志
//		uart_rec_buff[uart_rec_buff_cnt++] = SBUF;
		ring_buffer_write(SBUF0, &uart_rx_buf);
		// uart2_send_byte(SBUF0);
	}	
}
#endif


//串口2初始化
void uart2_init(u32 baud)
{
	MUX_SEL |= 0x40;//bit6置1表示将uart2接口引出到P0.4和P0.5
	P0MDOUT &= 0xCF;
	P0MDOUT |= 0x10;//设置对应的IO口输出输入
	ADCON = 0x80;//选择SREL0H:L作为波特率发生器
	SCON0 = 0x50;//接受使能和模式设置
	PCON &= 0x7F;//SMOD=0
	//波特率设置,公式为:
	//SMOD=0  SREL0H:L=1024-主频/(64*波特率),SMOD=1	 SREL0H:L=1024-主频/(32*波特率)
	baud = 1024-(u16)(3225600.0f/baud);
	SREL0H = (baud>>8)&0xff;  
	SREL0L = baud&0xff;
	
	#if(UART2_INT_EN)
		ES0 = 1;//中断使能
		EA = 1;

	#else
		ES0 = 0;
	#endif

}

//发送一个字节
void uart2_send_byte(u8 byte)
{
	ES0 = 0;//先关闭串口2中断
	SBUF0 = byte;
	while(!TI0);
	TI0 = 0;
	#if(UART2_INT_EN)
		ES0 = 1;//再打开中断
	#endif
}

//发送数据
void uart2_send_bytes(u8 *bytes,u16 len)
{
	u16 i;
	
	ES0 = 0;//先关闭串口2中断
	for(i=0;i<len;i++)
	{
		SBUF0 = bytes[i];
		while(!TI0);
		TI0 = 0;
	}
	#if(UART2_INT_EN)
		ES0 = 1;//再打开中断
	#endif
}


//用uart2串口实现printf函数
char putchar(char c)
{
	uart2_send_byte(c);
	
	return c;
}













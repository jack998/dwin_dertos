#ifndef DE_CONFIG_H__
#define DE_CONFIG_H__

/* Kernel Device Object */

// #define DE_USING_DEVICE
// #define DE_USING_CONSOLE
#define DE_CONSOLEBUF_SIZE 128
// #define DE_CONSOLE_DEVICE_NAME "uart1"

/* Command shell */

#define DE_USING_FINSH
#define FINSH_THREAD_NAME "deshell"
#define FINSH_USING_HISTORY
#define FINSH_HISTORY_LINES 5
#define FINSH_USING_SYMTAB
// #define FINSH_USING_DESCRIPTION
// #define FINSH_THREAD_PRIORITY 20
#define FINSH_THREAD_STACK_SIZE 4096
// #define FINSH_CMD_SIZE 80
#define FINSH_USING_MSH
#define FINSH_USING_MSH_DEFAULT
#define FINSH_USING_MSH_ONLY

#endif
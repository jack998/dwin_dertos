#ifndef __SCREEN_H__
#define __SCREEN_H__

#include "sys.h"

// enum base_graph_type_t{
//     GRAPH_POINT = 1,
//     GRAPH_LINE,
//     GRAPH_RECTANGLE_BOX,
//     GRAPH_RECTANGLE_FILL
// };

enum {
    TEXT_MASK_VALUE_ADDR = 0x00000001,
    TEXT_MASK_LOCATION_X = 0x00000002,
    TEXT_MASK_LOCATION_Y = 0x00000004,
};

typedef struct base_graph_param
{
    int16_t start_x;
    int16_t start_y;
    int16_t end_x;
    int16_t end_y;
    uint16_t color;
}base_graph_param_t;

void base_graph_rectangle_fill_set(uint16_t addr, base_graph_param_t base_graph_param_temp[], uint8_t len);
void text_display_value_set(uint16_t addr, uint16_t offset, char *text_data, uint16_t text_len);
void text_display_describe_set(uint16_t addr, uint32_t mask, uint16_t value_addr, int16_t x, int16_t y);

#endif

#include <T5LOS8051.H>
#include "sys.h"
#include "uart2.h"
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "shell.h"
#include "screen.h"
#include "touch.h"

#define PAGE_LINE_MAX   17  //一页最多十七行

uint8_t page_total = 0;         //总页数
uint8_t page_current = 0;       //当前页数
uint8_t last_page_lines = 0;    //最后一页的行数
uint8_t last_page_length = 0;   //最后一页的字符数
uint16_t page_addr = 0x2000;

void display_page_status()
{
    char page_temp[6] = {0};

    page_temp[0] = (page_current + 1) / 10 + '0';
    page_temp[1] = (page_current + 1) % 10 + '0';
    page_temp[2] = '/';
    page_temp[3] = (page_total + 1) / 10 + '0';
    page_temp[4] = (page_total + 1) % 10 + '0';
    text_display_value_set(0x1100, 0, page_temp, 3);
}

void shell_screen_append(uint8_t mode, char *line_data, uint8_t line_len)
{
    char line_temp[100] = {0};
    char line_cnt = 0;
    uint8_t i = 0;
    uint8_t current_line_cnt = 0;

    if(mode == 0)
    {
        memcpy(line_temp, "msh >", 5);
        line_cnt = 5;
    }
    if(page_current != page_total)
    {
        page_current = page_total;
        text_display_describe_set(0x1200, TEXT_MASK_VALUE_ADDR, page_addr + page_current * 0x200, 0, 0);
    }

    

    

    for(i = 0;i < line_len;i++)
    {
        line_temp[line_cnt++] = line_data[i];
    }

    current_line_cnt = (line_cnt / 25) + 1;
    last_page_lines += (line_cnt / 25) + 1;

    if(last_page_lines > PAGE_LINE_MAX)
    {
        page_total++;
        page_current = page_total;
        last_page_lines = current_line_cnt;
        last_page_length = 0;
        text_display_describe_set(0x1200, TEXT_MASK_VALUE_ADDR, page_addr + page_current * 0x200, 0, 0);
        display_page_status();
    }

    if(line_cnt % 2 == 1)
    {
        if(line_cnt % 25 < 23)
        {
            line_temp[line_cnt++] = ' ';
            line_temp[line_cnt++] = '\r';
            line_temp[line_cnt++] = '\n';
        }
        else if(line_cnt % 25 == 24)
        {
            line_temp[line_cnt++] = ' ';
        }
    }
    else
    {
        if(line_cnt % 25 < 24)
        {
            line_temp[line_cnt++] = '\r';
            line_temp[line_cnt++] = '\n';
        }
    }
//    printf("\r\n%s %d\r\n", line_temp, line_len);
    text_display_value_set(page_addr + page_current * 0x200, last_page_length / 2, line_temp, line_cnt / 2);
    last_page_length += line_cnt;
}

void page_next()
{
    // printf("page_next");
    if(page_current < page_total)
    {
        page_current++;
        text_display_describe_set(0x1200, TEXT_MASK_VALUE_ADDR, page_addr + page_current * 0x200, 0, 0);
        display_page_status();
    }
}

void page_last()
{
    // printf("page_last");
    if(page_current > 0)
    {
        page_current--;
        text_display_describe_set(0x1200, TEXT_MASK_VALUE_ADDR, page_addr + page_current * 0x200, 0, 0);
        display_page_status();
    }
}

void main()
{
    int ret = 0;
    char log_data[30] = {0};
    base_graph_param_t base_graph_param1;
    uint32_t timer_cnt = 0;

    sys_init();//系统初始化
    uart2_init(115200);
    shell_init();
    touch_init(0x1A00);
    touch_insert(0x0001, page_last);    //注册按钮上一页
    touch_insert(0x0002, page_next);    //注册按钮下一页
//	timer0_init();

//	t5l_write_rtc(rtc_cmd);

//	srand(10);
    
    base_graph_param1.start_x = 0;
    base_graph_param1.start_y = 238;
    base_graph_param1.end_x = 480;
    base_graph_param1.end_y = 238+251;
    base_graph_param1.color = 0x0000;
    base_graph_rectangle_fill_set(0x1000, &base_graph_param1, 1);

    while(1)
    {
        timer_cnt++;
        if(timer_cnt >= 3000)
        {
            timer_cnt = 0;
            if(base_graph_param1.start_y++ > base_graph_param1.end_y)
            {
                break;
            }
            base_graph_rectangle_fill_set(0x1000, &base_graph_param1, 1);
        }
    }

//    sys_delay_ms(10);
//    uart2_send_bytes("start Shell!\r\n", sizeof("Start shell!\r\n") - 1);
    
	while(1)
	{
        shell_task_entry();
        touch_handler();
	}
}